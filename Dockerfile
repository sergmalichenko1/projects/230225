FROM golang:1.20.1 AS builder
WORKDIR /build
COPY . .
#RUN cd outyet && go build 
RUN cd outyet && CGO_ENABLED=0 go build -o app && cp ./app ../



FROM alpine:3.18.0
RUN apk --no-cache add ca-certificates
WORKDIR /app
COPY --from=builder /build/app ./
EXPOSE 8080
CMD ["./app"]

